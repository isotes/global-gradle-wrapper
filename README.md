# Global Gradle Wrapper
Use the [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) without the repo noise:
- [install](#Install) this [Global Gradle Wrapper](https://gitlab.com/isotes/global-gradle-wrapper/-/releases) once and add it to your search PATH
- keep project specific `gradle/wrapper/gradle-wrapper.properties` files
- remove (and add to gitignore): `gradlew`, `gradlew.bat`, and `gradle/wrapper/gradle-wrapper.jar` from your projects
- use the Global Gradle Wrapper `gw` instead of `./gradlew` anywhere within your Gradle projects


## Why
I like specifying the Gradle version as part of a project as a foundation for reproducible builds, but dislike adding binary files to repositories and the noise introduced by the scripts in the root directory of repositories. Additionally, just calling `gw` from anywhere in a Gradle project is more convenient than using the wrapper scripts.

Finally, since IntelliJ IDEA only requires the `gradle-wrapper.properties` file, this approach works for both the command line and the IDE I use for Java development.


## How
This project just contains a [small patch](wrapper.patch) to the [main class](https://github.com/gradle/gradle/blob/master/subprojects/wrapper/src/main/java/org/gradle/wrapper/GradleWrapperMain.java) of the upstream project that changes how the `gradle/wrapper/gradle-wrapper.properties` file is found. The original code locates the properties file relative to the `gradle-wrapper.jar`. This projects starts from the current repository and searches upwards for `gradle/wrapper/gradle-wrapper.properties`, comparably to how Git finds the root directory of a repository.

Additionally, the [start scripts patch](start-scripts.patch) changes the lookup for the `gradle-wrapper.jar` file as follows
- for `gradlew.bat`, look in the directory of the script for `global-gradle-wrapper.jar`
- for `gradlew`, look first relative to the script in `../share/global-gradle-wrapper/global-gradle-wrapper.jar` (which assumes a `bin` & `share` directory structure) and fall back to the directory of the script


## Install
There are 3 different installation packages available [here](https://gitlab.com/isotes/global-gradle-wrapper/-/releases):
- global-gradle-wrapper-windows.zip for Windows, containing
	- global-gradle-wrapper.jar
	- gw.bat
- global-gradle-wrapper-posix-opt.tgz for Posix operating systems having the gw script and the jar file in the same directory, containing
	- global-gradle-wrapper/global-gradle-wrapper.jar
	- global-gradle-wrapper/gw
- global-gradle-wrapper-posix-usr.tgz for installing in a Posix usr directory structure, containing
	- share/global-gradle-wrapper/global-gradle-wrapper.jar
	- bin/gw


## Build
Run `./build.sh GRADLE-TAG-OR-BRANCH`, e.g., `./build.sh v7.6.0`, which performs the following in the `tmp` subdirectory
- clones the [Gradle repo](https://github.com/gradle) (shallow, for the specified tag)
- applies the patches
- builds the wrapper.jar
- creates the dist directory with the packages as described above
- runs the generated packages against the example versions in the [tests](tests) directory


## Disclaimer
This project is not associated with the [Gradle project](https://gradle.org/).

## License
[Apache 2.0](LICENSE)
