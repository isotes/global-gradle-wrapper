#!/bin/sh
set -eu

[ "${1-}" ] || { echo "Missing required positional parameter Gradle git repository tag, e.g., 'v7.6.0'"; exit 2; }
tag="$1"

scriptdir="$(cd "${0%/*}" >/dev/null 2>&1 && pwd)"

dest="${2-tmp}"
rm -rf "$dest/dist" "$dest/tests"
mkdir -p "$dest/dist"
repo="$dest/gradle"

if [ -r "$repo/gradlew" ]; then
	echo "Repository already cloned at '$repo': skipping clone"
else
	git clone --depth 1 --branch "$tag" https://github.com/gradle/gradle.git "$repo"
fi

cd "$repo"
if git apply --reverse --check < "$scriptdir/wrapper.patch"; then
	echo "wrapper.patch already applied: skipping"
else
	git apply < "$scriptdir/wrapper.patch"
fi

# can't overwrite the originals
mkdir -p build/global-gradle-wrapper
cp gradlew gradlew.bat build/global-gradle-wrapper
git apply --directory=build/global-gradle-wrapper < "$scriptdir/start-scripts.patch"

echo "== Building"
# we don't care about the tests; ignore Java version to allow compiling with JDK > 11
# disable daemon and toolchain download as there are issues when run within docker (for CI)
./gradlew :wrapper:build \
	-x test -x embeddedIntegTest -x embeddedCrossVersionTest \
	-Porg.gradle.java.installations.auto-download=false -Dorg.gradle.daemon=false \
	--no-configuration-cache --no-build-cache --no-scan --stacktrace \
	-Dorg.gradle.ignoreBuildJavaVersionCheck=true

echo "== Packaging"
cd ../dist
mkdir -p windows posix-opt/global-gradle-wrapper posix-usr/bin posix-usr/share/global-gradle-wrapper
for d in windows posix-opt/global-gradle-wrapper posix-usr/share/global-gradle-wrapper; do
	cp ../gradle/subprojects/wrapper/build/libs/gradle-wrapper.jar "$d/global-gradle-wrapper.jar"
done
cp ../gradle/build/global-gradle-wrapper/gradlew.bat windows/gw.bat
for d in posix-opt/global-gradle-wrapper posix-usr/bin; do
	cp ../gradle/build/global-gradle-wrapper/gradlew "$d/gw"
	chmod 755 "$d/gw"
done

zip -qj global-gradle-wrapper-windows.zip windows/*
for d in posix-opt posix-usr; do
	tar -czf "global-gradle-wrapper-$d.tgz" -C "$d" .
done

echo "== Testing"
cd ..
cp -r "$scriptdir/tests" "tests"
cd tests
for d in *.*; do
	cd "$d"
	../../dist/posix-usr/bin/gw --version | grep "Gradle $d"
	../../dist/posix-opt/global-gradle-wrapper/gw --version | grep "Gradle $d"
	cd ..
done

echo "== Done"
